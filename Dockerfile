FROM python:3.8
LABEL authors="heptagon"

WORKDIR /
COPY . /app
WORKDIR /app

RUN pip3 install -r /app/requirements.txt

#COPY ./app /app/app
#RUN alembic upgrade head

EXPOSE 8000

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "5000"]