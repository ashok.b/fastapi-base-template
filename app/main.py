from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.api import v1
from app.config import settings

app = FastAPI()

if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(v1.router, prefix="/api/v1")


@app.get("/")
def hello():
    return "App is up and running!!!"
