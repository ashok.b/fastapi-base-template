from fastapi import APIRouter

router = APIRouter()


@router.get(
    "/ping",
    summary="Ping Route",
    description="Ping route, returns pong!.",
)
def pong():
    return {"ping": "pong!"}
