# FastAPI-base-template

This will serve as a template for creating FastAPI projects.


## Getting started

Clone this repo into your project repo.

## Directory Structure

```bash
├── app
│   ├── api
│   │   ├── v1
│   │   │   ├── routes
│   │   │   ├── crud
│   ├── db
│   │   ├── migrations
│   │   │   ├── versions
│   │   ├── models
│   ├── tests
│   ├── __init__.py
│   ├── config.py
│   └── main.py
├── Dockerfile
├── docker-compose.yml
├── alembic.ini
└── requirements.txt
```


